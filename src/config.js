const Sequelize = require('sequelize');


module.exports = class config {
    constructor ({database, username, password, host, dialect, port, logging, pool, define, timezone, benchmark, native}){
        let sq = new Sequelize(database, username, password, { host, dialect, port, timezone, define, pool, logging, benchmark, native });
        return sq;
    }
};