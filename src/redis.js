const redis = require('redis');

class Redis {
    constructor({host, port, prefix, db, enable}){
        this.enable = enable;
        this.redis = redis.createClient({host, port, prefix, db});
    }

    get(key) {
        if (!this.enable) return false;
        return new Promise((res, rej) => {
            this.redis.get(key, (err, val) => {
                if (err !== null)
                    rej(err);
                let json_value = JSON.parse(val);
                if(typeof json_value == 'object')
                    res(json_value);
                else
                    res(val);
            });
        });
    }

    del(key) {
        if (!this.enable) return false;
        return new Promise((res, rej) => {
            this.redis.del(key, (err, val) => {
                if (err !== null)
                    rej(err);
                res(val == 1);
            });
        });
    }

   mset(arr_data){
        if (!this.enable) return false;
        return new Promise((res, rej) => {
            let action = this.redis.multi().mset(arr_data);
            action.exec((err, resp) => {
                if (err) rej(err);
                if (!resp) rej();
                if (resp[0] == 'OK')
                    res(true);
                res(false);
            });
        });
    }

    set(key, value, time=null) {
        if (!this.enable) return false;
        return new Promise((res, rej) => {
            let action = this.redis.multi().set(key, JSON.stringify(value));
            if (time)
               action.expire(key, time);
            action.exec((err, resp) => {
                if (err) rej(err);
                if (!resp) rej();
                if (time){
                    if (resp[0] == 'OK' && resp[1] == 1)
                        res(true);
                    this.redis.expire(key, 0);
                    res(false);
                }else{
                    if (resp[0] == 'OK')
                        res(true);
                    res(false);
                }
            });
        });
    }

    getTtl (key) {
        if (!this.enable) return false;
        return new Promise( (res, rej) => {
            this.redis.ttl(key, (err, resp) => {
                if (err) rej(err);
                if (!resp) res({});
                res(resp);
            });
        });
    }

    sadd (key, arr_value){
        if (!this.enable) return false;
        return new Promise((res, rej) => {
            this.redis.sadd(key, arr_value, (err, resp)=>{
                if (err) rej(err);
                res(resp);
            });
        });
    }

    hmgetall (key){
        if (!this.enable) return false;
        return new Promise((res, rej) => {
            this.redis.hgetall(key, (err, resp) => {
                if (err) rej(err);
                if (!resp) res({});
                res(resp);
            });
        });
    }

    hmset (key, value={}, time=null){
        if (!this.enable) return false;
        return new Promise((res, rej) => {
            let action = this.redis.multi().HMSET(key, value);
            if (time)
                action.expire(key, time);
            action.exec((err, resp) => {
                if (err) rej(err);
                if (!resp) rej();
                if (time){
                    if (resp  && resp[0] == 'OK' && resp[1] == 1)
                        res(true);
                    redis.expire(key, 0);
                    res(false);
                }else{
                    if (resp[0] == 'OK') res(true);
                    else res(false);
                }
            });
        });
    }

    hget (key, field){
        if (!this.enable) return false;
        return new Promise((res, rej) => {
            this.redis.hget(key, field, (err, resp) => {
                if (err) rej(err);
                res(resp);
            });
        });
    }

    hdel (key, field){
        if (!this.enable) return false;
        return new Promise((res, rej) => {
            this.redis.hdel(key, field, (err, resp) => {
                if (err) rej(err);
                res(resp);
            });
        });
    }
}

module.exports = Redis;