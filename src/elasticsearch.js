const {ElasticBaseQuery, ElasticConfig, Elasticquery, ElasticqueryStatic} = require('dy-elasticsearch');


class Elastic extends ElasticBaseQuery {
    constructor({servers}){
        super();
        if(servers.length == 0)
            throw {code: "empty", message: "servers is empty"};
        this._servers = {};
        this._server = servers[0].name;
        for (let server of servers)
            this._servers[server.name] = new ElasticConfig({node: server.url, maxRetries: 5, requestTimeout: 60000, error: true, prefix: server.prefix});
        return this;
    }
    init(data){
        this._index = data.index;
        this._doctype = data.doctype || null;
        this._filter_must = data.filter_must || [];
        this._filter_should = data.filter_should || [];
        this._filter_must_not = data.filter_must_not || [];
        this._sort = data.sort || [];
        this._aggs = data.aggs || {};
        this._start = data.start || 0;
        this._size = data.size || 10;
        this._search_after = data.search_after || null;
        this._minimum_should_match = data.minimum_should_match || null;
        this.query_extend = data.query_extend || {};
        this._server = data.server || this._server;
        return this;
    }

    getEsIndex(index){
        return ElasticqueryStatic.getIndex({ index, server: this._servers[this._server] });
    }

    async search(options = {}) {
        return await Elasticquery(this).search({options, server: this._servers[this._server]});
    }

    async msearch({body , index_as_key = true, raw = false }) {
        return await ElasticqueryStatic.msearch({ server: this._servers[this._server], body, index_as_key, search_type:'query_then_fetch', raw });
    }

    async set(id, data) {
        return await Elasticquery(this).set({id, data, server: this._servers[this._server]});
    }
    
    async delete(id) {
        return await Elasticquery(this).delete({ id, refresh: true, server: this._servers[this._server] });
    }

    async get(id, options = {}) {
        return await Elasticquery(this).get({ id, options, server: this._servers[this._server]});
    }

    async exists(id) {
        return await Elasticquery(this).exists({id, server: this._servers[this._server]});
    }

    async count() {
        return await Elasticquery(this).count({server: this._servers[this._server]});
    }

    async aggregate() {
        return await Elasticquery(this).aggregate({ server: this._servers[this._server] });
    }

    async searchFirst(options = {}) {
        return await Elasticquery(this).searchFirst({options, server: this._servers[this._server]});
    }

    async deleteIndex() {
        return await ElasticqueryStatic.deleteIndex({ index: this._index, server: this._servers[this._server]});
    }

    async esBulk(data, refresh=true){
        return await ElasticqueryStatic.esBulk({ data, refresh, server: this._servers[this._server]});
    }

    async indicesSetup(index, data) {
        return await ElasticqueryStatic.indicesSetup({index, map: data, server: this._servers[this._server]});
    }

    async indicesExists(index) {
        return await ElasticqueryStatic.indicesExists({index, server: this._servers[this._server]});
    }
}

module.exports = Elastic;