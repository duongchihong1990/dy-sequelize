const { Sequelize, Model, DataTypes } = require("sequelize");
const moment = require("moment-timezone");
const _ = require("lodash");

class BaseModel {
    static _init ({name, schema, schemaOptions, connection, currentModel, elasticsearch, redis}){
        class Base extends Model{}
        let model = Base.init(schema, Object.assign({sequelize: connection, modelName: name}, schemaOptions));
        model.afterSave(async (data, options) => {
            if (!options.cacheEnable) return;

            // data model
            let modelData = await currentModel.resModel({data: data.dataValues});

            // elastic update
            if (options.esCacheEnable && elasticsearch){
                await currentModel.mapping();
                let {esIndex, esDoctype} = currentModel,
                    id = await currentModel.getCacheId({data: data.dataValues}),
                    _e;
                _e = await elasticsearch.init({index: esIndex, doctype: esDoctype}).set(id, modelData);
                console.log(`AfterSaveToEs: ${currentModel.name} Id: ${id} Result: ${JSON.stringify(_e)}`);
            }

            // redis update
            if (options.redisCacheEnable && redis ){
               let redisData, rdIndex = await currentModel.redisIndex({data: data.dataValues}); 
               if (!rdIndex) return;
               redisData = await redis.set(rdIndex, modelData);
               console.log(`AfterSaveToRs: ${currentModel.name} Key: ${rdIndex} Result: ${JSON.stringify(redisData)}`);
            }
        });
        model.beforeDestroy(async (data, options) => {
            if (!options.cacheEnable) return;

            // elastic delete
            if (options.esCacheEnable && elasticsearch){
                let {esIndex, esDoctype} = currentModel,
                    id = await currentModel.getCacheId({data: data.dataValues}),
                    _e;
                _e = await elasticsearch.init({index: esIndex, doctype: esDoctype}).delete(id);
                console.log(`BeforeDestroyEs: ${currentModel.name} Id: ${id} Result: ${JSON.stringify(_e)}`);
            }

            // redis delete
            if (options.redisCacheEnable && redis ){
                let redisData, rdIndex = await currentModel.redisIndex({data: data.dataValues}); 
                if (!rdIndex) return;
                redisData = await redis.del(rdIndex);
                console.log(`BeforeDestroyRs: ${currentModel.name} Key: ${rdIndex} Result: ${JSON.stringify(redisData)}`);
            }
        });

        return model;
    }
    constructor({model, connection, elasticsearch, redis}){ 
        this.name = `${model.name.toLowerCase()}`;
        this.schema = BaseModel.schema({data: model.schema()});
        this.schemaOptions = model.schemaOptions();
        this.resModel = model.resModel;
        this.connection = connection;
        this.sequelize = Sequelize;
        this.dataTypes = DataTypes;
        this.elasticsearch = elasticsearch;
        this.redis = redis;
        this.model = model._init({ name: this.name, schema: this.schema, schemaOptions: this.schemaOptions, connection, currentModel: this, elasticsearch, redis});
        return this;
    }
    get table () {return this.model;}
    get esIndex (){return this.name;}
    get esDoctype (){return this.name;}
    async mapping (){}
    async redisIndex ({data}) { return data.id; }
    defaultOption({options}){return Object.assign({ raw: false, cacheEnable: true, esCacheEnable: true, redisCacheEnable: false, updateDate: {'field': 'write_date'}, individualHooks: true, allowUpdateNull: false}, options || {});}
    async getCacheId ({data}) { return data.id; }
    static get sequelize (){return Sequelize;}
    static get dataTypes (){return DataTypes;}
    static schema ({data, merge=true}) {
        if (!merge)
            return schema;
        return Object.assign({
            id: {type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true},
            uuid: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},
            create_date: { type: 'TIMESTAMP', defaultValue: DataTypes.NOW},
            write_date: { type: 'TIMESTAMP', defaultValue: DataTypes.NOW},
            create_uid: { defaultValue: 1, validate: {isNumeric: {msg: 'Must be an integer'}}, type: DataTypes.INTEGER}, 
            write_uid: { defaultValue: 1, validate: {isNumeric: {msg: 'Must be an integer'}}, type: DataTypes.INTEGER}
        }, data || {});
    }

    static schemaOptions(indexes=[]) {
        return Object.assign({}, {
            timestamps: false, createdAt : 'create_date',
            updatedAt : 'write_date', underscored: true,
            indexes: _.union(indexes, [{name: `uuid_index`, fields: ['uuid'], unique: true}]),
        });
    }

    checkConnection(){
        return new Promise((res, rej) => {
            this.connection.authenticate().then(() => {
                res('Connection has been established successfully.');
            }).catch(err => {
                res('Unable to connect to the database:', err);
            });
        });
    }

    async resModel ({data}, parseTime=true){
        if (!parseTime) return data;
        let create_date, write_date;
        create_date = moment.parseZone(data.create_date, moment.ISO_8601).format("x")*1;
        write_date = moment.parseZone(data.write_date, moment.ISO_8601).format("x")*1;
        return Object.assign({}, data, {create_date, write_date});
    }

    async _create({data, options={}}) {
        options = this.defaultOption({options});
        if (!data) return false;
        let res = await this.model.create(data, options);
        if (options.raw)
            return res.dataValues;
        return await this.resModel({data: res.dataValues});
    }

    async _count(){
        return await this.model.count();
    }
    async _countWhere({conditions={}}){
        return await this.model.count(conditions);
    }
    async _findAll({conditions={}, raw=false}){
        let data = await this.model.findAll(conditions),
            result = [];
        for (let item of data){
            if (raw){ result.push(item.dataValues); continue;}
            result.push(await this.resModel({data: item.dataValues}));
        }
        return result;
    }

    async _findByPk({id, raw=false}){
        let data = await this.model.findByPk(id);
        if (data === null)
            return false;
        if (raw)
            return data.dataValues;
        return await this.resModel({data: data.dataValues});
    }


    async _getWhere({conditions={}, raw=false}) {
        let data = await this.model.findOne(conditions);
        if (data === null)
            return false;
        if (raw)
            return data.dataValues;
        return await this.resModel({data: data.dataValues});
    }

    async _getWheres({conditions={}, raw=false}) {
        let data = await this.model.findAll(conditions),
            result = [];
        for (let item of data){
            if (raw){ result.push(item.dataValues); continue;}
            result.push(await this.resModel({data: item.dataValues}));
        }
        return result;
    }

    async _bulkCreate({data, options}){
        let result, res = [];
        options = this.defaultOption({options});
        if (!data) return false;
        result = await this.model.bulkCreate(data, options);
        for (let item of result){
            if (options.raw) { res.push(item.dataValues); continue;}
            res.push(await this.resModel({data: item.dataValues}));
        }
        return res;
    }
    
    async _unlink({data, options={}}){
        return Boolean(await this.model.destroy(Object.assign(data, this.defaultOption({options}))));
    }

    async _update({data, conditions={}, options={}}){
        let _k = {}, res = [];
        if (!conditions) return false;
        options = this.defaultOption({options});
        if (!options.allowUpdateNull)
            Object.keys(data).map((key)=>(data[key] == null) && delete data[key]);
        if (options.updateDate){
            _k[options.updateDate.field] = moment.utc().format('YYYY-MM-DD HH:mm:ss');
            data = Object.assign(data, _k);
        }
        let [total, result] = await this.model.update(data, Object.assign(conditions, options));
        if (total == 0)
            return res;
        for (let item of result){
            if (options.raw) { res.push(item.dataValues); continue;}
            res.push(await this.resModel({data: item.dataValues}));
        }
        return res;
    }

    async prepareElasticBulkData({rawdata}){
        let bulk_data = [], data;
        for (let i of rawdata){
            data = await this.resModel({data: i});
            bulk_data.push({'index': {
                '_index': this.elasticsearch.getEsIndex(this.esIndex),
                '_type': this.esDoctype,
                '_id': await this.getCacheId({data})}});
            bulk_data.push(data);
        }
        return bulk_data;
    }

    async prepareRedisBulkData({rawdata}){
        let bulk_data = [], data;
        for (let i of rawdata){
            data = await this.resModel({data: i});
            bulk_data.push(await this.redisIndex({data: i}));
            bulk_data.push(JSON.stringify(data));
        }
        return bulk_data;
    }

    async recacheAll({step=5}){
        let count = 0, label, loop, offset, sqlData, limit, data, result, defaultOption = this.defaultOption();
        count = await this._count();
        loop = Math.ceil(count/step);
        if(!defaultOption.cacheEnable)
            return false;
        label = `RecacheAll ${this.elasticsearch.getEsIndex(this.esIndex)}`;
        console.time(label);
        while (loop > 0){
            loop -= 1;
            offset = step*loop;
            limit = step;
            data = await this._getWheres({conditions:{ offset, limit }});
            if(defaultOption.esCacheEnable){
                result = await this.elasticsearch.esBulk(await this.prepareElasticBulkData({rawdata: data}));
                console.log(`RecacheEsAll: ${this.elasticsearch.getEsIndex(this.esIndex)} Offset: ${offset} Limit: ${limit} Took: ${result.took} Errors: ${result.errors}`);
            }
            if(defaultOption.redisCacheEnable){
                result = await this.redis.mset(await this.prepareRedisBulkData({rawdata: data}));
                console.log(`RecacheRdAll: ${this.name} Result ${result}`);
            }
        }
        console.timeEnd(label);

        return true;
    }
}

module.exports = BaseModel;